#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
#include "HelloWorld/HelloWorldPubSubTypes.h"
#include <ostream>
#include <signal.h>

volatile sig_atomic_t Stop;
using namespace eprosima::fastdds::dds;

// Callback function for alternative two in subscription creation
void OnDataAvailableCallback(const HelloWorld &msg, const SampleInfo& info) {
    std::cout << msg.message() << " Number is " << msg.aNumber() << std::endl;
}

class TestClass
{
public:
    TestClass(const char* name) : _name(name) {}

    void onDataAvailable(const HelloWorld& msg, const SampleInfo& info)
    {
        std::cout << "Sent: " << info.source_timestamp << " received: " << info.reception_timestamp
            << " - message: \"" << msg.message()
            << "\" - number: " << msg.aNumber()
            << " - from " << _name << std::endl;
    }
    void onSubscriptionMatched(const SubscriptionMatchedStatus& info)
    {
        if (info.current_count_change > 0)
            std::cout << "Subscriber \"" << _name << "\" matched\n";
        else if (info.current_count_change < 0)
            std::cout << "Subscriber \"" << _name << "\" unmatched\n";
        else
            std::cout << "Nothing happened in" << _name;
    }

private:
    const char* _name;
};

// Compile options
enum class ESubMode { Lambda, Callback, ObjectCallback };
// constexpr ESubMode SubMode = ESubMode::Lambda;
constexpr ESubMode SubMode = ESubMode::ObjectCallback;

int main()
{
    std::cout << "Starting subscriber example. Press STOP to quit cleanly" << std::endl;

    DomainParticipant* participant;
    DdsSubscriber<HelloWorldPubSubType>* subscriber;

    TestClass testClass("Test number one");

    signal(SIGTERM, [](int signum) { Stop = 1; });
    // Create node
    DomainParticipantQos participantQos;
    participantQos.name("Participant_publisher");
    participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    subscriber = new DdsSubscriber<HelloWorldPubSubType>(participant, "HelloWorld");

    if (participant == nullptr)
    {
        return 1;
    }
    // Create Subscriber
    if constexpr (SubMode == ESubMode::Lambda)
    {
        subscriber->bindDataAvailableCallback([=](const HelloWorld& msg, const SampleInfo& info) {
            std::cout << "Sent: " << info.source_timestamp << " received: " << info.reception_timestamp
            << " Delta: " << (info.reception_timestamp - info.source_timestamp) << " " << msg.message()
            << " Number is " << msg.aNumber() << std::endl;
        });
    }
    else if constexpr (SubMode == ESubMode::Callback)
    {
        subscriber->bindDataAvailableCallback(&OnDataAvailableCallback);
    }
    else if constexpr (SubMode == ESubMode::ObjectCallback)
    {
        subscriber->bindDataAvailableCallback(&testClass, &TestClass::onDataAvailable);
        subscriber->bindSubscriptionMatchedCallback(&testClass, &TestClass::onSubscriptionMatched);
    }
    else
    {
        std::cout << "No valid subscription mode set. Aborting\n";
        Stop = true;
    }

    while (!Stop)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    delete subscriber;
    DomainParticipantFactory::get_instance()->delete_participant(participant);
    std::cout << "Exited cleanly";
    return 0;
}
