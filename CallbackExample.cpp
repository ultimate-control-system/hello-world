#include <iostream>
#include <functional>
#include <utility>

class BaseClass
{
public:
    void update(const char* msg)
    {
        onCallback(msg);
    }

protected:
    virtual void onCallback(const char* msg)
    {
        (void)msg;
    }
};

class ChildClass : public BaseClass
{
    void onCallback(const char* msg) override
    {
        std::cout << msg << " - through virtual class override\n";
    }
};

void callback(const char* msg)
{
    std::cout << msg << " - through callback function\n";
}

class WorkerClass
{
public:
    WorkerClass(BaseClass* virtualClass, std::function<void(const char* msg)> callback) :
        _virtualClass(virtualClass)
        , _callback(std::move(callback))
    {}

    void update()
    {
        _virtualClass->update("Worker class calling callback");
        _callback("Worker class calling callback");
    }

private:
    BaseClass* _virtualClass;
    std::function<void(const char*)> _callback;
};

int main() {
    ChildClass childClass;

    WorkerClass workerClass(&childClass, callback);

    workerClass.update();

    return 0;
}
