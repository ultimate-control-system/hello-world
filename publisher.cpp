
#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
#include "HelloWorld/HelloWorldPubSubTypes.h"
#include <ostream>

using namespace eprosima::fastdds::dds;

DomainParticipant *participant_;
DdsPublisher<HelloWorldPubSubType> *publisher;


int main() {
    std::cout << "Starting Publisher" << std::endl;

    DomainParticipantQos participantQos;
    participantQos.name("Participant_publisher");
    participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (participant_ == nullptr) {
        return 1;
    }

    // Create publisher
    publisher = new DdsPublisher<HelloWorldPubSubType>(participant_, "HelloWorld");

    HelloWorld hello_world;

    for (int i = 0; i < 100; ++i) {
        hello_world.aNumber() += 1.23;
        hello_world.message() = "Hello World!";
        publisher->Publish(hello_world);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    delete publisher;
    publisher = nullptr;
    DomainParticipantFactory::get_instance()->delete_participant(participant_);
    return 0;
}
