cmake_minimum_required(VERSION 3.12.4)

if(NOT CMAKE_VERSION VERSION_LESS 3.0)
    cmake_policy(SET CMP0048 NEW)
endif()

project(hello_world)

# Find requirements

# Set C++17
set(CMAKE_CXX_STANDARD 17)
include(CheckCXXCompilerFlag)
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANG OR
        CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    check_cxx_compiler_flag(-std=c++17 SUPPORTS_CXX17)
    if(SUPPORTS_CXX17)
        add_compile_options(-std=c++17)
    else()
        message(FATAL_ERROR "Compiler doesn't support C++17")
    endif()
endif()


add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../library ${CMAKE_CURRENT_SOURCE_DIR}/library)

add_executable(
    hello_world_publisher
    publisher.cpp
)
target_link_libraries(
    hello_world_publisher
    DDSLibrary
)
add_executable(
    hello_world_subscriber
    subscriber.cpp
)
target_link_libraries(
    hello_world_subscriber
    DDSLibrary
)

add_executable(
        CallbackExample
        CallbackExample.cpp
)

# Copy DDSLibrary.dll if windows
if (WIN32)
    add_custom_command(
            TARGET hello_world_subscriber POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${CMAKE_CURRENT_SOURCE_DIR}/library/DDSLibrary.dll
            ${PROJECT_BINARY_DIR}/DDSLibrary.dll
    )
endif()